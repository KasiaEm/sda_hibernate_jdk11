package com.sda.hibernate;

import com.sda.hibernate.entity.Employee;
import com.sda.hibernate.entity.Phone;
import com.sda.hibernate.entity.Project;
import com.sda.hibernate.entity.Task;
import com.sda.hibernate.enums.PhoneBrand;
import com.sda.hibernate.enums.TaskType;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;

public class SimpleLazyGet {
    private static Employee employee1 = null;
        private static final SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        private static EntityManager entityManager = sessionFactory.createEntityManager();

    public static void main(String[] args) {

        generateData();
        loadTasks();
        loadTasksJoinFetch();

        HibernateUtils.shutdown();
    }

    public static void generateData(){
        entityManager = sessionFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Project project = new Project();
        employee1 = new Employee();
        Employee employee2 = new Employee();
        Employee employee3 = new Employee();
        Task task1 = new Task("t1", "text1", TaskType.BLOCKER, employee1);
        Task task2 = new Task("t2", "text2", TaskType.HIGH_PRIORITY, employee1);
        Task task3 = new Task("t3", "text3", TaskType.NORMAL, employee2);
        Task task4 = new Task("t4", "text4", TaskType.BLOCKER, employee1);
        Task task5 = new Task("t5", "text5", TaskType.NORMAL, employee2);
        Task task6 = new Task("t6", "text6", TaskType.HIGH_PRIORITY, employee3);
        Task task7 = new Task("t7", "text7", TaskType.NORMAL, employee3);
        Phone phone1 = new Phone("123", PhoneBrand.LG);
        Phone phone2 = new Phone("5", PhoneBrand.SAMSUNG);
        Phone phone3 = new Phone("6", PhoneBrand.SAMSUNG);

        project.setProjectName("Small project");

        employee1.setFirstName("Jane");
        employee1.setFamilyName("Doe");
        employee1.setEmail("jdoe@gmail.com");
        employee1.getProjects().add(project);
        employee1.setPhone(phone1);

        employee2.setFirstName("Jake");
        employee2.setFamilyName("Smith");
        employee2.setEmail("jsmith@gmail.com");
        employee2.getProjects().add(project);
        employee2.setPhone(phone2);

        employee3.setFirstName("Simon");
        employee3.setFamilyName("Black");
        employee3.setEmail("sblack@gmail.com");
        employee3.getProjects().add(project);
        employee3.setPhone(phone3);

        entityManager.persist(project);
        entityManager.persist(employee1);
        entityManager.persist(employee2);
        entityManager.persist(employee3);
        entityManager.persist(task1);
        entityManager.persist(task2);
        entityManager.persist(task3);
        entityManager.persist(task4);
        entityManager.persist(task5);
        entityManager.persist(task6);
        entityManager.persist(task7);
        entityManager.persist(phone1);
        entityManager.persist(phone2);
        entityManager.persist(phone3);
        entityManager.flush();

        entityManager.getTransaction().commit();

        Employee employeeFromDB = entityManager.find(Employee.class, 1);
        entityManager.close();
        System.out.println("TASKS SIZE outside transaction" + employeeFromDB.getTasks().size());
    }

    public static void loadTasks(){
        entityManager = sessionFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Employee employeeFromDB = entityManager.find(Employee.class, 1);
        System.out.println("TASKS SIZE inside transaction" + employeeFromDB.getTasks().size());

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public static void loadTasksJoinFetch(){
        entityManager = sessionFactory.createEntityManager();
        Employee employeeFromDB = entityManager.createQuery("select e from Employee e " +
                "join fetch e.tasks " +
                "where e.id = :id", Employee.class)
                .setParameter("id", 1)
                .getSingleResult();
        System.out.println("TASKS SIZE from join fetch" + employeeFromDB.getTasks().size());
        entityManager.close();
    }
}
