package com.sda.hibernate;

import com.sda.hibernate.entity.Task;
import com.sda.hibernate.enums.TaskType;
import com.sda.hibernate.service.TaskService;

public class Main {
    private static TaskService ts = null;

    public static void main(String[] args) {
        Task task = new Task("First task", "Short description", TaskType.NORMAL, null);
        ts = new TaskService();
        ts.addTask(task);
        ts.close();
    }
}
