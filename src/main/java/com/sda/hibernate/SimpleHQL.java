package com.sda.hibernate;

import com.sda.hibernate.entity.Employee;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class SimpleHQL {
    public static void main(String[] args) {
        final SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        final EntityManager entityManager = sessionFactory.createEntityManager();

        entityManager.getTransaction().begin();

        /*Query query=entityManager.createQuery("insert into Employee (firstName, familyName, email) " +
                "select firstName, familyName, email from Employee where id=:id");
        query.setParameter("id", 1);
        int rowsCopied=query.executeUpdate();*/

        /*Query query=entityManager.createQuery("delete from Employee where id=:id");
        query.setParameter("id", 2);
        int rowsDeleted=query.executeUpdate();*/

        /*Query query2=entityManager.createQuery("update Employee set firstName = 'Jake' where id=:id");
        query2.setParameter("id", 3);
        int rowsModified=query2.executeUpdate();*/

        /*Query query = entityManager.createQuery("from Employee as e where e.id=:id");
        query.setParameter("id", 1);
        List<Employee> employeeList = query.getResultList();*/

        /*Query query = entityManager.createQuery("from Employee");
        query.setFirstResult(0);
        query.setMaxResults(2);
        List<Employee> results = query.getResultList();
        results.forEach(x-> System.out.println(x.getFirstName()));*/

        entityManager.getTransaction().commit();

        HibernateUtils.shutdown();
    }
}
