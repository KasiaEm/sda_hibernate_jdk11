package com.sda.hibernate.dao;

import com.sda.hibernate.HibernateUtils;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;

public class GenericDaoImpl<T, Id extends Serializable> implements GenericDao<T, Id>{
    private Class<T> type;
    private SessionFactory sf = null;
    private EntityManager em = null;

    public GenericDaoImpl(Class<T> type) {
        this.type = type;
    }

    @Override
    public void create(T o) {
        em.persist(o);
    }

    @Override
    public T read(Id id) {
        return (T) em.find(type, id);
    }

    @Override
    public void update(T o) {
        em.merge(o);
    }

    @Override
    public void delete(T o) {
        em.remove(o);
    }

    @Override
    public void openSessionFactory() {
        sf = HibernateUtils.getSessionFactory();
    }

    @Override
    public void closeSessionFactory() {
        HibernateUtils.shutdown();
    }

    public void openTransaction(){
        em = sf.createEntityManager();
        em.getTransaction().begin();
    }

    public void closeTransaction(){
        em.getTransaction().commit();
        em.close();
    }


}
