package com.sda.hibernate.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDao <T, Id extends Serializable>{
    void create(T newInstance);

    T read(Id id);

    void update(T transientObject);

    void delete(T persistentObject);

    void openSessionFactory();

    void closeSessionFactory();

    void openTransaction();
    void closeTransaction();
}
