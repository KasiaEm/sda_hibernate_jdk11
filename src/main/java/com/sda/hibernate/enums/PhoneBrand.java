package com.sda.hibernate.enums;

public enum PhoneBrand {
    SAMSUNG, APPLE, LG
}
