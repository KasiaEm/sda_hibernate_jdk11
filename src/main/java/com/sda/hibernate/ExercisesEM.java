package com.sda.hibernate;

import com.sda.hibernate.entity.Employee;
import com.sda.hibernate.entity.Task;
import com.sda.hibernate.enums.PhoneBrand;
import com.sda.hibernate.enums.TaskType;
import org.h2.engine.User;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class ExercisesEM {
    private static final SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
    private static EntityManager entityManager = sessionFactory.createEntityManager();

    public static void main(String[] args) {
        //loading tasks pages
        /*Integer line = 0;
        while (!line.equals(-1)) {
            System.out.println("Type tasks page nr: ");
            line = new Scanner(System.in).nextInt();
            if (line != -1)
                loadTaskPage(line);
        }*/

        //filter employees by phone brand
        //filterEmployeesByPhoneBrand(PhoneBrand.SAMSUNG);

        //delete employees with no tasks
        //generateEmployeesWithNoTasks();
        //deleteEmployeesWithNoTasks();

        //update block queries
        //updateBlockerQueries();

        //assign tasks to another user
        removeTasksFromUser(1);

        //update corp email addresses
        //assignCorpMailAddress("sda.com");

        HibernateUtils.shutdown();
    }

    private static void generateEmployeesWithNoTasks() {
        entityManager.getTransaction().begin();

        Employee employee1 = new Employee();
        employee1.setFirstName("Robert");
        employee1.setFamilyName("Grey");
        employee1.setEmail("rgrey@gmail.com");

        Employee employee2 = new Employee();
        employee2.setFirstName("Tina");
        employee2.setFamilyName("Snow");
        employee2.setEmail("tsnow@gmail.com");

        entityManager.persist(employee1);
        entityManager.persist(employee2);

        entityManager.getTransaction().commit();
    }

    public static void filterEmployeesByPhoneBrand(PhoneBrand phoneBrand) {
        List<Employee> employees = entityManager.createQuery("select e from Employee e " +
                                                                "join fetch e.phone " +
                                                                "where e.phone.phoneBrand = :brand", Employee.class)
                .setParameter("brand", phoneBrand)
                .getResultList();
        System.out.println("Users by brand: " + phoneBrand.name());
        employees.forEach(x-> System.out.println(x.getFirstName() + " " + x.getFamilyName() + " " + x.getPhone().getPhoneBrand().name()));
    }

    public static void deleteEmployeesWithNoTasks() {
        entityManager.getTransaction().begin();

        int rowsDeleted=entityManager.createQuery("delete from Employee e " +
                                                    "where e.tasks is empty")
                                    .executeUpdate();
        System.out.println("Employees deleted: " + rowsDeleted);

        entityManager.getTransaction().commit();
    }

    public static void updateBlockerQueries() {
        entityManager.getTransaction().begin();

        List<Task> results = entityManager.createQuery("from Task t where t.taskType = :type", Task.class)
                .setParameter("type", TaskType.BLOCKER).getResultList();
        for(Task t:results){
            t.setTitle("BLOCKER! " + t.getTitle());
            entityManager.persist(t);
        }
        System.out.println("Tasks modified: " + results.size());
        entityManager.getTransaction().commit();
    }

    public static void loadTaskPage(int pageNr) {
        System.out.println("Tasks page nr " + pageNr);
        List<Task> results = entityManager.createQuery("from Task", Task.class)
                .setFirstResult(pageNr * 3)
                .setMaxResults(3)
                .getResultList();
        results.forEach(x -> System.out.println(x.getTitle() + " " + x.getDescription()));
    }

    public static void removeTasksFromUser(Integer employeeId){
        entityManager = sessionFactory.createEntityManager();
        entityManager.getTransaction().begin();
        Employee employeeOld = entityManager.createQuery("select e from Employee e " +
                "join fetch e.tasks t " +
                "where e.id = :id", Employee.class)
                .setParameter("id", employeeId)
                .getSingleResult();
        System.out.println("Tasks selected to remove: ");
        Set<Task> tasksToAssign = employeeOld.getTasks();
        tasksToAssign.forEach(x-> System.out.println(x.getTitle()));

        List<Employee> result = entityManager.createQuery("select e from Employee e "
                                + "left join fetch e.tasks t "
                                + "group by e.id order by count(t) asc", Employee.class)
        .setMaxResults(1).getResultList();
        Employee employee = result.get(0);
        System.out.println("Employee with least tasks: " + employee.getFirstName() + " " + employee.getFamilyName());
//        for(Task t : tasksToAssign){
//            t.setEmployee(employee);
//            entityManager.persist(t);
//        }
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public static void assignCorpMailAddress(String newDomain){
        entityManager = sessionFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List<Employee> employees = entityManager.createQuery("from Employee e where e.email not like lower(concat(e.firstName, '.', e.familyName, '@', :newDomain))", Employee.class)
                .setParameter("newDomain", newDomain)
                .getResultList();
        System.out.println("Employees with invalid email address: ");
        employees.forEach(e-> System.out.println(e.getFirstName()+ " " + e.getFamilyName() + " " + e.getEmail()));
        for (Employee e :employees){
            e.setEmail(e.getFirstName().toLowerCase()+"."+e.getFamilyName().toLowerCase()+"@"+newDomain);
            entityManager.persist(e);
        }
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}