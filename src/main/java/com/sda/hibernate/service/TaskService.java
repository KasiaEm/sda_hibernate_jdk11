package com.sda.hibernate.service;

import com.sda.hibernate.dao.GenericDao;
import com.sda.hibernate.dao.GenericDaoImpl;
import com.sda.hibernate.entity.Task;

public class TaskService {
    private GenericDao gd = null;

    public TaskService(){
        gd = new GenericDaoImpl(Task.class);
        gd.openSessionFactory();
    }

    public void addTask(Task task){
        gd.openTransaction();
        gd.create(task);
        gd.closeTransaction();
    }

    public void close(){
        gd.closeSessionFactory();
    }
}
