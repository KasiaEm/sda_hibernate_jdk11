package com.sda.hibernate.entity;

import com.sda.hibernate.enums.PhoneBrand;

import javax.persistence.*;

@Entity
@Table(name = "PHONE")
public class Phone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;

    @Column(name = "model_nr")
    private String modelNr;

    @Column(name = "phone_brand")
    @Enumerated(EnumType.STRING)
    private PhoneBrand phoneBrand;

    @OneToOne(mappedBy = "phone", fetch = FetchType.EAGER)
    private Employee employee;

    public Phone() {

    }

    public Phone(String modelNr, PhoneBrand phoneBrand) {
        this.modelNr = modelNr;
        this.phoneBrand = phoneBrand;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModelNr() {
        return modelNr;
    }

    public void setModelNr(String modelNr) {
        this.modelNr = modelNr;
    }

    public PhoneBrand getPhoneBrand() {
        return phoneBrand;
    }

    public void setPhoneBrand(PhoneBrand phoneBrand) {
        this.phoneBrand = phoneBrand;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
